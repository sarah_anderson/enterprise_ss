package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// Login
func login(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "OwnerID, Content-Type")
	body, _ := ioutil.ReadAll(r.Body)
	var userSignIn UserSignIn
	err := json.Unmarshal(body, &userSignIn)
	if err != nil {
		return "Failed to login", nil
	}

	if isValidUsernameAndPassword(userSignIn.Username, userSignIn.Password) {
		appUser := generateAppUser(userSignIn.Username)
		fmt.Println("Login Success")
		return appUser, nil
	}
	fmt.Println("Failed authentication")
	return "Failed authentication", nil
}
 
