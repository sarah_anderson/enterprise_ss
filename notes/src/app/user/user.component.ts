import { Component, OnInit } from '@angular/core';
import { ViewbagService, ApplicationUser } from '../viewbag.service';
import { NgForm } from '@angular/forms';
import { Router } from "@angular/router";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {
  appUser: ApplicationUser
  currentUser: string;
  username: string = "";
  password: string = "";
  error: string = "";


  loading: boolean = false;

  constructor(private viewbagService: ViewbagService, private router: Router) {   
    this.currentUser = this.viewbagService.getUsername(); 
    if (!this.currentUser ){
      this.router.navigate(['/'])
    }
      this.appUser = this.viewbagService.getAppUser();
      this.currentUser = this.viewbagService.getUsername();
   }

}
