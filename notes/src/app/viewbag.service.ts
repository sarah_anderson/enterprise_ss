import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ViewbagService {
  appUser: ApplicationUser;

  constructor(private http: Http) { }

  signin(username: string, password: String) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    var body = {
      Username: username,
      Password: password,
    }

    return this.http.post('http://localhost:8080/login', body)
      .map(response => this.appUser = this.unmarhsalAppUser(response.json()));  
  }

  logout() {
    this.appUser = null;
  }

  private handleError(error: any): Promise<any> {
    console.log('An error occurred'); // for demo purposes only
    console.log(error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  getUsername() {
    if (this.appUser == null){
      return "";
    }
    return this.appUser.username;
  }

  isUserAdmin() {
    if (this.appUser == null){
      return false;
    }
    return this.appUser.isAdmin;
  }

  getUserID() {
    if (this.appUser == null){
      return -1;
    }
    return this.appUser.userId;
  }

  getToken() {
    if (this.appUser == null){
      return "";
    }
    return this.appUser.token;
  }

  getAppUser(){
    return this.appUser;
  }

  public unmarhsalAppUser(data) {
    console.log("Logging in: ")
    console.log( data )
    return new ApplicationUser(data.UserId, data.Username, data.IsAdmin, data.Token);
  }
}


export class ApplicationUser {
  userId: number;
  username: string;
  isAdmin: boolean;
  token: string;

  constructor(userId: number, username: string, isAdmin: boolean, token: string) {
    this.userId = userId;
    this.username = username;
    this.isAdmin = isAdmin;
    this.token = token;
  }
}