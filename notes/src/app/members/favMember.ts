import { Injectable } from '@angular/core';

export class FavMember {
    name: string;    
    friendId: number;
    memberID: number;
    canRead: boolean;
    canWrite: boolean;
    access: number;
    
    constructor(name: string = "", friendId: number = -1, canRead: boolean = false, canWrite: boolean= false) {
        this.name = name;
        this.friendId = friendId;
        this.canRead = canRead;
        this.canWrite = canWrite;
        this.setAccess()
    }

    public setOwner(id: number){
        this.memberID = id;
        this.setAccess()
    }

    public setAccess(){
        if (this.canWrite){
            this.access = 2 
        }
        else if (this.canRead){
            this.access = 1 
        }
        else{
            this.access = 0
        }
    }
}
