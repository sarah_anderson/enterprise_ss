import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MembersComponent } from './members.component';
import { MembersRoutingModule } from './members-routing.module';
import { Member } from './member'
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, MembersRoutingModule, FormsModule],
  declarations: [MembersComponent],
  exports: [MembersComponent]
})
export class MembersModule { }
