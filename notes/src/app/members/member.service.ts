import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { Member, CreateMemberDetails, FavAccess } from './member'
import { FavMember } from './favMember'
import { ViewbagService } from '../viewbag.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class MemberService {
  memberList: Member[] = [];
  memberFavList: FavMember[] = [];
  ownerID: number

  constructor(private http: Http, private viewbagService: ViewbagService) { 
    this.ownerID = this.viewbagService.getUserID()
  }

  public getFavMemberList(userID: number): Observable<FavAccess[]> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'name' : userID, 'OwnerID' : userID });
    let options = new RequestOptions({ headers: headers });
    return this.http.get("http://localhost:8080/favourite/list", options)
                    .map(response => response.json());
  }  

  public getMemberList(): Observable<Member[]> {
    let headers = new Headers({ 'Content-Type': 'application/json', 'OwnerID': this.ownerID });
    let options = new RequestOptions({ headers: headers });
    return this.http.get('http://localhost:8080/member/list', options)
                    .map(response => response.json());
  }

  public deleteFavMember(favMember: FavMember){
    let headers = new Headers({ 'Content-Type': 'application/json', 'OwnerID' : this.ownerID });
    let options = new RequestOptions({ headers: headers });
    var req = this.http.post('http://localhost:8080/favourite/delete', favMember, options);
    req.subscribe();
  }

  public updateFavMember(favMember: FavMember){ 
    let headers = new Headers({ 'Content-Type': 'application/json', 'OwnerID' : this.ownerID });
    let options = new RequestOptions({ headers: headers });
    favMember.setAccess();
    console.log(favMember.access)
    var req = this.http.post('http://localhost:8080/favourite/edit', favMember, options);
    req.subscribe();
  }

  public createFavMember(favMember: FavMember){
    let headers = new Headers({ 'Content-Type': 'application/json', 'OwnerID' : this.ownerID });
    let options = new RequestOptions({ headers: headers });
    var req = this.http.post('http://localhost:8080/favourite/create', favMember, options);
    req.subscribe();
  }

  public createMember(member: CreateMemberDetails) {
    let headers = new Headers({ 'Content-Type': 'application/json', 'OwnerID' : this.ownerID });
    let options = new RequestOptions({ headers: headers });
    var req = this.http.post('http://localhost:8080/member/create', member, options);
    req.subscribe();
  }

  private handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || {};
  }
  private handleErrorObservable(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

  public unmarhsalFavMembers(data) {    
    for (let member of data) {
      var aMember = new Member(member.FriendName, member.FriendID, member.Access);
      this.memberFavList.push( 
        new FavMember(
          aMember.name, 
          aMember.id, 
          aMember.defaultPrivilege >= 1,
          aMember.defaultPrivilege == 2
        )
      );
    }
  }

  public getAccessEnum(canRead: boolean, canWrite: boolean): number{
    if (canRead && canWrite){
      return 2
    }

    if (canRead){
      return 1
    }

    return 0
  }
  

  private handleError(error: any): Promise<any> {
    console.log('An error occurred'); // for demo purposes only
    console.log(error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
