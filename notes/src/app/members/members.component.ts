import { Component, OnInit } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Member, CreateMemberDetails } from './member'
import { MemberService } from './member.service';
import { NgForm } from '@angular/forms';
import { Router } from "@angular/router";
import { ViewbagService } from 'app/viewbag.service';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Component({
  moduleId: module.id,
  selector: 'app-members',
  templateUrl: 'members.component.html',
  styleUrls: ['members.component.css']
})
export class MembersComponent implements OnInit {
  currentUser: string;
  currentUserIsAdmin: boolean;
  memberList: Member[] = [];
  newMember: CreateMemberDetails = new CreateMemberDetails();

  constructor(private memberService: MemberService, private viewbagService: ViewbagService, private router: Router) {
    this.currentUser = this.viewbagService.getUsername();
    this.currentUserIsAdmin = this.viewbagService.isUserAdmin();
    if (!this.currentUser || !this.currentUserIsAdmin) {
      this.router.navigate(['/'])
    }
    this.memberList = [];

  }

  ngOnInit() {
    this.getMembers()
  }

  private getMembers() {
   this.memberService.getMemberList().subscribe(
      (data)=>{       
        this.unmarhsalMembers(data);
      }
    );
  }

  private unmarhsalMembers(data) {
    if (data) {
      for (let member of data) {
        this.memberList.push(new Member(member.Name, member.MemberID, member.AccountType));
      }
    }
  }

  private createMember() {
    //this.newMember =  new CreateMemberDetails( );
    var tempNewMember = new Member(this.newMember.name, -1, 0)
    this.memberList.push(tempNewMember) // Done client side, not full fresh
    this.newMember.setAccessRights();
    this.memberService.createMember(this.newMember);
    this.getMembers(); // Full refresh of the member list
    this.closeModal()
  }

  private closeModal() {
    this.newMember.name = "";
    this.newMember.password = "";
    this.newMember.isAdminSelected = false;
  }
}