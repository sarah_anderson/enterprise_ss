import { Injectable } from '@angular/core';

export class FavAccess{
    MemberID: number;
    FriendID: number;
    FriendName: string;
    Access: number;

    constructor(memberID: number, friendID: number, friendName: string, access: number) {
        this.MemberID = memberID;
        this.FriendID = friendID;
        this.FriendName = friendName;
        this.Access = access;
    }

}

export class Member {
    name: string;    
    id: number;
    defaultPrivilege: number;

    constructor(name: string, id: number, defaultPrivilege: number) {
        this.name  = name;
        this.id = id;
        this.defaultPrivilege = defaultPrivilege;
    }

    isReadOnly = function(): boolean{
        return this.defaultPrivilege == 1;
    }

    hasRead = function(): boolean{
        return this.defaultPrivilege >= 1;
    }

    hasReadWrite = function(): boolean{
        return this.defaultPrivilege == 2;
    }

    hasNoAccess = function(): boolean{
        return this.defaultPrivilege == 0;
    }

}

export class CreateMemberDetails {
    name: string;    
    password: string;
    isAdmin: number;
    isAdminSelected: boolean

    /*constructor(name: string, password: string, isAdmin: boolean) {
        this.name  = name;
        this.password = password;
        this.isAdmin = isAdmin;
    }*/

    constructor(){

    }

    setAccessRights(){
        if (this.isAdminSelected){
            this.isAdmin = 1
        }
        else{
            this.isAdmin = 0
        }
    }
}
