import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Member, FavAccess } from '../../members/member'
import { FavMember } from '../../members/favMember'
import { NotePrivilege, NoteEditDetails } from './noteEditDetails'
import { ViewbagService } from '../../viewbag.service';
import { MemberService } from '../../members/member.service';
import { NoteService } from '../note.service';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Router } from "@angular/router";

declare var $: any;

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})

export class CreateComponent implements OnInit {
  currentUser: string;
  warning: string = "";
  favHistory: FavAccess[] = [];
  userList: Member[] = [];

  viewModel: NotePrivilege[] = [];
  text: string;
  ownerID: number;

  defaultRead: FavAccess[] = [];
  defaultWrite: FavAccess[] = [];
  defaultNoAccess: FavAccess[] = [];

  constructor(private viewbagService: ViewbagService, private memberSerivice: MemberService, private noteSerivice: NoteService, private router: Router) {   
    this.currentUser = this.viewbagService.getUsername(); 
    if (!this.currentUser ){
      this.router.navigate(['/'])
    }
    this.warning += this.viewbagService.getUsername()
    this.ownerID = this.viewbagService.getUserID()
  }

  populateSelectLists() {
    this.favHistory.forEach(favMem => {
      if (favMem.Access == 2) {
        this.defaultWrite.push(favMem);
      }
      else if (favMem.Access == 1) {
        this.defaultRead.push(favMem);
      }
      else {
        this.defaultNoAccess.push(favMem);
      }
    });
  }

  createNote() {
    
    var write = this.getValues("#write-multi-select")
    var read = this.getValues("#read-multi-select")
    var note = new NoteEditDetails()
    note.setNoteDetails(this.ownerID, this.text, new Date(), read, write, "", "", false)
    this.noteSerivice.createNote( note )
  }

  ngOnInit(): void {
    this.memberSerivice.getFavMemberList(this.ownerID).subscribe(
      (data) => {
        this.favHistory = data;
        this.initDropDowns()
        this.populateSelectLists()
      },
      (err) => { },
      () => {
        setTimeout(() => this.updateSelect2(), 1) // Force it wait till everything is done
      }
    );
  }

  initDropDowns() {
    $("#read-multi-select").select2({
      placeholder: 'Select users to read',
      allowClear: true,
      width: '30em'
    });

    $("#read-multi-select").on('change', (e, args) => {
      console.log(e);
    });

    $("#write-multi-select").select2({
      placeholder: 'Select users to write',
      allowClear: true,
      width: '30em'
    });

    $("#write-multi-select").on('change', (e, args) => {
      this.getLastSelect("#write-multi-select");
    });

    $('#read-multi-select').on('change', (e, args) => {
      this.getLastSelect("#read-multi-select");
    });
  }

  updateSelect2() {
    $('#write-multi-select').trigger('change');
    $('#read-multi-select').trigger('change.select2');
  }

  getValues(multiSelectId: string): number[] {
    var selectedValues = [];
    $(multiSelectId).find("option:selected").each(function () {
      var id: number;
      id = Number($(this).val())
      selectedValues.push(id);
    });
    return selectedValues;
  };
  
  thisWselected(int: number){
    console.log("Changed: ", int)
  }
  getLastSelect(multiSelectId: string) {
    var readId = "#read-multi-select";
    var writeId = "#write-multi-select";
    var old_values = [];
    var multiSelect = $(multiSelectId);
    var id;
    multiSelect.on("select2:select", function (event) {
      var values = [];
      // copy all option values from selected
      $(event.currentTarget).find("option:selected").each(function (i, selected) {
        values[i] = $(selected).val();
      });
      // doing a diff of old_values gives the new values selected
      id = $(values).not(old_values).get();

      //let selected: number[] = [];
      console.log("Id", id)
      if (multiSelectId == writeId) {
        var selectedWrite = [];
        multiSelect.find("option:selected").each(function (i, selected) {
          selectedWrite = $(selected).val();
        });
        console.log("Select: " + selectedWrite)
        var hasDuplicate = selectedWrite.includes(id);
        // warn the user
        if (hasDuplicate) {
          this.warning = "The user with the id " + id + "has conflicting privileges."
          console.log(this.warning);
        }
      }

      if (multiSelectId == readId) {
        var allSelected = [];
        multiSelect.find("option:selected").each(function (i, selected) {
          allSelected = $(selected).val();
        });

        var hasDuplicate = allSelected.indexOf(id.toString()) > -1;
        console.log(hasDuplicate, id.toString())
        console.log("allSelected: " + allSelected)
        // warn the user
        if (hasDuplicate) {
          this.warning = "The user with the id " + id + "has conflicting privileges."
          console.log(this.warning);
        }
      }
      console.log('validated');
    });
  }
/*
  validateChange(id: number, name: string) {
    var readId = "#read-multi-select";
    var writeId = "#write-multi-select";
    let selected: number[] = [];

    console.log(name);
    if (name == writeId) {
      selected = this.getValues(readId)
      var hasDuplicate = selected.includes(id);
      // warn the user
      this.warning = "The user with the id " + hasDuplicate + "has conflicting privileges."
      console.log(this.warning);
    }


    if (name == readId) {
      selected = this.getValues(writeId)
      var hasDuplicate = selected.includes(id);
      // warn the user
      this.warning = "The user with the id " + hasDuplicate + "has conflicting privileges."
      console.log(this.warning);
    }
  }*/
}
/*

  updateViewModel(id, privilege) {
    //this.viewModel.getElementByID(id)
    // Create is not exists

    // Update privilege
  }

  
}*/


