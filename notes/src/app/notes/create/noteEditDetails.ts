import { Injectable } from '@angular/core';

export class NotePrivilege {   
    id: number;
    privilege: number;
  
    constructor (id: number, privilege: number){
      this.id = id;
      this.privilege = privilege;
    }
  }

  export class Note {
    Text: string;
    CreatedDateTime: Date;
    OwnerID: number;
    NoteID: number;

    constructor(text: string, createdDateTime: Date, ownerID: number) {
        this.Text = text;
        this.CreatedDateTime  = createdDateTime;
        this.OwnerID = ownerID;
    }
    
    setNoteID(noteID: number){
      this.NoteID = noteID
    }
}


  export class NoteEditDetails {   
    NewNote: Note;
    owner: string;
    isOwner: boolean;
    ReadOnlyUsersIDs: number[];
    ReadWriteUsersIDs: number[];
    NoAccessUserIDs: number[];
    canWrite: boolean;
  
    constructor (){
      var empty: number[]
      this.NewNote = new Note("", new Date(), -1);
      this.ReadOnlyUsersIDs = empty;
      this.ReadWriteUsersIDs = empty;
      this.owner = "";
    }
    
    setNoteDetails(noteID: number, text: string, createdDateTime: Date, read: number[], write: number[], owner: string, username: string, canWrite: boolean){
      this.NewNote.setNoteID(noteID);
      this.NewNote = new Note(text, createdDateTime, -1);
      this.ReadOnlyUsersIDs = read;
      this.ReadWriteUsersIDs = write;
      this.owner = owner;
      this.isOwner = this.owner == username;
    }

    public addNoAccess(ID: number){
      this.NoAccessUserIDs.push(ID)
    }
  }