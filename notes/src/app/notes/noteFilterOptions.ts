import { Injectable } from '@angular/core';

export class NoteFilterOptions {
    preSufFixText: string;    
	asPrefix: boolean;
	asSuffix: boolean;

	numbers: number;
	isConsecutive: boolean;

	emailDomain: string;

	searchPresetWords: boolean; // nto used

    capWord3OrMore: boolean; // not used

    constructor(preSufFixText: string, asPrefix: boolean, asSuffix: boolean, 
        numbers: number, isConsecutive: boolean, 
        emailDomain: string, searchPresetWords: boolean, capWord3OrMore: boolean) {
        this.preSufFixText = preSufFixText;
        this.asPrefix = asPrefix;
        this.asSuffix = asSuffix;
        this.numbers = numbers;
        this.isConsecutive = isConsecutive;    
        this.emailDomain = emailDomain;
        this.searchPresetWords = searchPresetWords;
        this.capWord3OrMore = capWord3OrMore;
    }
}


export class RegexFilter{
    regex: string;
    message: string;
    filterOption: number;

    constructor( regex: string, message: string,filterOption: number){
        this.regex = regex;
        this.message =  message;
        this.filterOption =  filterOption;
    }
}