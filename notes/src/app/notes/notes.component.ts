import { Component, AfterViewInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NoteService } from './note.service';
import { NoteListItem } from './NoteListItem';
import { NoteFilterOptions, RegexFilter } from './NoteFilterOptions';
import { NoteEditDetails } from './create/noteEditDetails';
import { Member, FavAccess } from './../members/member';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { Router } from "@angular/router";
import { ViewbagService } from 'app/viewbag.service';
import { MemberService } from 'app/members/member.service';
declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements AfterViewInit  {
  editNote: NoteEditDetails;
  userID: number;
  currentUser: string;
  noteList: NoteListItem[] = [];
  noteFilterOptions: NoteFilterOptions;
  test: any;
  sessionID: string;

  selectedFilters: RegexFilter[];
  filterTypes: string[];
  selectedFilter: number;

  defaultRead: FavAccess[] = [];
  defaultWrite: FavAccess[] = [];
  defaultNoAccess: FavAccess[] = [];

  constructor(private noteService: NoteService, private viewbagService: ViewbagService, private memberService: MemberService,private router: Router) {
    var empty: number[];
    this.userID = this.viewbagService.getUserID();
    this.currentUser = this.viewbagService.getUsername();
    this.editNote = new NoteEditDetails();

    if (!this.currentUser) {
      this.router.navigate(['/']);
    }

    this.refreshFilters();   
  }

  ngAfterViewInit() {
   this.updateAccess()
  }

  updateAccess(){
    this.noteList.forEach(element => {
      element.updateAccess(this.currentUser);
    });
  }

  refreshFilters(){   
    this.selectedFilters = [];
    this.sessionID = (Date.now() / 1000).toString();
    this.noteFilterOptions = new NoteFilterOptions("", false, false, 0, false, "", false, false);

    this.noteService.clearFilterSession();
    this.noteService.getNoteListItemList(this.noteFilterOptions, this.sessionID, true).subscribe(
      (data) => {
        this.noteList = this.noteService.unmarhsalNoteListItems(data);
      },
      () =>{
        this.updateAccess()
      }
    );
    this.initFilters()
  }
  getNoteEditDetails(note: NoteListItem) { // Called from HTML
    note.updateAccess(this.currentUser);
    var read: number[] = [];
    var write: number[] = [];
    
    this.memberService.getFavMemberList(this.userID).subscribe(
      (data) => {
        console.log(data)
        data.forEach(element => {
          if (element.Access == 1) {
            this.defaultRead.push(element);
            read.push(element.FriendID)
          }
          if (element.Access == 2) {
            this.defaultWrite.push(element);
            write.push(element.FriendID)
          }
          if (element.Access == 0) {
            this.defaultNoAccess.push(element);
          }
     
        });
      },
      (err) => { },
      () => {
        console.log(note)
        this.editNote = new NoteEditDetails()
        this.editNote.setNoteDetails(note.noteID, note.content, new Date(), read, write, note.owner, this.currentUser, note.canWrite);
        setTimeout(() => this.initDropDowns(), 1) // Force it wait till everything is done 
      }

    );

  }

  initDropDowns() {
    $("#read-multi-select").select2({
      placeholder: 'Select users to read',
      allowClear: true,
      width: '30em'
    });

    $("#write-multi-select").select2({
      placeholder: 'Select users to write',
      allowClear: true,
      width: '30em'
    });
  }


  clear() {   
    this.selectedFilter = -1;
    this.noteFilterOptions = new NoteFilterOptions("", true, true, 0, true, "", true, true);
    this.selectedFilters = [];
    this.initFilters();

    this.sessionID = new Date().getTime().toString();
    this.noteService.clearFilterSession();
  }

  filter() {
    console.log("Todo: Make a service for notes: And Filter")
    console.log("Session: ", this.sessionID)
    var regexFilterObj = this.getFilterOptionsForSelected(this.noteFilterOptions);
    this.selectedFilters.push(regexFilterObj);
    this.selectedFilters.forEach(element => {
      this.noteService.getNoteListItemList(this.noteFilterOptions, this.sessionID,  this.selectedFilters.length == 1).subscribe(
        (data) => {
          this.noteList = this.noteService.unmarhsalNoteListItems(data);
        }
      );
    });
  }

  getFilterOptionsForSelected(options): RegexFilter {
    var regex: string = "";
    var message: string = "";
    var filterOption: number = this.selectedFilter;
    if (filterOption == 0) {
      message = "A sentence with " + options.preSufFixText + " as a ";
      if (options.asPrefix && !options.asSuffix) {
        regex = ""
        message += "prefix."
      }
      else if (options.asPrefix && options.asSuffix) {
        regex = ""
        message += "prefix and suffix."
      } else {
        regex = ""
        message += "suffix."
      }
    }
    else if (filterOption == 1) {
      message = options.numbers.toString() + " as a phone number area code"

      if (options.isConsecutive) {
        regex = ""
        message += " and consecutively anywhere."
      }
      else {
        regex = ""
        message += "."
      }
    }
    if (filterOption == 2) {
      regex = ""
      message = "Email address partially containing " + options.emailDomain + " in the domain."
    }
    if (filterOption == 3) {
      regex = ""
      message = "Text that contains at least three of the following case-insensitive words: meeting, minutes, agenda, action, attendees, apologies"
    }
    if (filterOption == 4) {
      regex = ""
      message = "A word in all capitals of three characters or more."
    }
    return new RegexFilter(regex, message, filterOption)
  }

  initFilters() {
    this.filterTypes = [];
    this.filterTypes.push("Prefix / Suffix in a sentence");
    this.filterTypes.push("Numbers in phone number and/or consecutively");
    this.filterTypes.push("Email address partially containing this in the domain");
    this.filterTypes.push("Contains at least three preset words");
    this.filterTypes.push("A word in all capitals of three characters or more");
   
   
  }


  updateNote() {
    console.log("TO DO: Update this note")
    console.log(this.editNote)
    this.noteService.editNote(this.editNote)
  }

}
