import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NotesComponent } from './notes.component';
import { CreateComponent } from './create/create.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'notes', component: NotesComponent },
      { path: 'notes/create', component: CreateComponent }
    ])
  ],
  exports: [RouterModule]
})
export class NotesRoutingModule { }
