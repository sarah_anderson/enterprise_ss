import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotesComponent } from './notes.component';
import { CreateComponent } from './create/create.component';
import { NotesRoutingModule } from './notes-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule ({
  imports: [CommonModule, NotesRoutingModule, FormsModule],
  declarations: [NotesComponent, CreateComponent],
  exports: [NotesComponent,CreateComponent]
})
export class NotesModule { }
