import { Injectable } from '@angular/core';

export class NoteListItem {
    content: string;    
    owner: string;
    noteID: number;
    count: number;
    access: number;
    isOwner: boolean;
    canRead: boolean;
    canWrite: boolean;
    

    constructor(owner: string, content: string, noteID: number, access: number, count: number, username: string) {
        this.owner = owner;
        this.content  = content;
        this.noteID = noteID;
        this.count =  count;
        this.access = access;
        this.isOwner = this.owner == username;
        this.canRead = this.access >= 1;
        this.canWrite = this.access == 2
    }

    updateAccess(username: string){
        this.isOwner = this.owner == username;
        this.canRead = this.access >= 1;
        this.canWrite = this.access == 2
    }

}
