import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { NoteListItem } from './noteListItem'
import { NoteFilterOptions } from './NoteFilterOptions';
import { NoteEditDetails } from './create/noteEditDetails';
import { ViewbagService } from 'app/viewbag.service';
import { FavMember } from '../members/favMember';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class NoteService {
  noteListItemList: NoteListItem[] = [];
  ownerID: number
  appUsername: string

  constructor(private http: Http, private viewbagService: ViewbagService) {
    this.ownerID = this.viewbagService.getUserID();
    this.appUsername = this.viewbagService.getUsername();
  }

  public createNote(noteDetails: NoteEditDetails) {
    let headers = new Headers({ 'Content-Type': 'application/json', 'OwnerID': this.ownerID });
    let options = new RequestOptions({ headers: headers });
    var req = this.http.post('http://localhost:8080/notes/create', noteDetails, options);
    req.subscribe();

  }

  public editNote(noteDetails: NoteEditDetails) {
    let headers = new Headers({ 'Content-Type': 'application/json', 'OwnerID': this.ownerID });
    let options = new RequestOptions({ headers: headers });
    var req = this.http.post('http://localhost:8080/notes/edit', noteDetails, options);
    req.subscribe();

  }

  public clearFilterSession() {
    let headers = new Headers({ 'Content-Type': 'application/json', 'OwnerID': this.ownerID });
    let options = new RequestOptions({ headers: headers });
    this.http.get('http://localhost:8080/notes/clear', options);
  }

  // GET: Note List w/ search parameters
  public getNoteListItemList(noteFilterOptions: NoteFilterOptions, sessionID: string, isNewQuery: boolean): Observable<NoteListItem[]> {
    this.ownerID = this.viewbagService.getUserID();
    let headers = new Headers({ 'Content-Type': 'application/json', 'OwnerID': this.ownerID });
    let options = new RequestOptions({ headers: headers });
    return this.http.get('http://localhost:8080/notes/list?' +
      'preSufFixText=' + noteFilterOptions.preSufFixText +
      '&asPrefix=' + noteFilterOptions.asPrefix +
      '&asSuffix=' + noteFilterOptions.asSuffix +
      '&numbers=' + noteFilterOptions.numbers +
      '&isConsecutive=' + noteFilterOptions.isConsecutive +
      '&emailDomain=' + noteFilterOptions.emailDomain +
      '&searchPresetWords=' + noteFilterOptions.searchPresetWords +
      '&capWord3OrMore=' + noteFilterOptions.capWord3OrMore +
      '&sessionID=' + sessionID + 
      '&isNewQuery=' + isNewQuery
      , options
      
    )
      .map(response => response.json());
  }


  private handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body.data || {};
  }
  private handleErrorObservable(error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }

  public unmarhsalNoteListItems(data): NoteListItem[] {
    var notes: NoteListItem[] = []
    if (data) {
      for (let noteListItem of data) {
        notes.push(new NoteListItem(noteListItem.MemberName, 
          noteListItem.NoteText, noteListItem.Note_ID, noteListItem.Access, 
          noteListItem.OccCount, this.appUsername));
      }
    }
    return notes
  }

  private handleError(error: any): Promise<any> {
    console.log('An error occurred'); // for demo purposes only
    console.log(error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
