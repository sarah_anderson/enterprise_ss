import { Component, ViewChild, OnInit, ElementRef } from '@angular/core';
declare var $: any;
import { Member } from '../members/member'
import { FavMember } from '../members/favMember'
import { MemberService } from '../members/member.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { ViewbagService } from 'app/viewbag.service';
import { Router } from "@angular/router";


@Component({
  moduleId: module.id,
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent {
  currentUser: string;
  ownerID: number;
  memberList: FavMember[] = [];

  editMemberId: number = -1;
  editCanRead: boolean = false;
  editCanWrite: boolean = false;
  newFav: FavMember;

  constructor(private memberService: MemberService, private viewbagService: ViewbagService, private router: Router) {
    this.currentUser = this.viewbagService.getUsername();
    this.ownerID = this.viewbagService.getUserID()
    if (!this.currentUser) {
      this.router.navigate(['/'])
    }
    
    this.newFav = new FavMember();
    this.memberService.getFavMemberList(this.ownerID)
      .subscribe(
      (data) => {
        this.convertToMember(data)
      },
      (err) => { },
      () => {
        setTimeout(() => this.InitSliders(), 1) // Force it wait till everything is done
      }
      );


    $("#membersNotFavourites-select").select2();
  }

  createFav() {
    // TO DO: Validate the user
    this.memberService.createFavMember(this.newFav);
  }

  convertToMember(data) {
    if (data) {
      console.log(data)
      for (let member of data) {
        if (member.FriendID != this.ownerID) {
          this.memberList.push(
            new FavMember(
              member.FriendName,
              member.FriendID,
              member.Access >= 1,
              member.Access == 2
            )
          );
        }
      }
    }
  }

  InitSliders() {
    this.memberList.forEach(m => {
      this.initSingleSlider(m)
    });
  }

  initSingleSlider(m: FavMember) {
    $('#read' + m.friendId).bootstrapToggle({
      width: '150px'
    }).prop('checked', m.canRead).change()
      .change(function () {
        this.editCanRead = $(this).prop('checked');
       // console.log(this.editCanRead)
        //console.log("Employee " + m.friendId + "'s read has changed to " + $(this).prop('checked' + this.editCanRead))
      })

    $('#write' + m.friendId).bootstrapToggle({
      width: '150px'
    }).prop('checked', m.canWrite).change()
      .change(function () {
        this.editCanWrite = $(this).prop('checked')
        //console.log("Employee " + m.friendId + "'s write has changed to " + $(this).prop('checked') + this.editCanWrite)
      })
  }

  toggle(m: FavMember) {
    this.initSingleSlider(m)
    this.InitSliders()
    this.editCanRead = m.canRead;
    this.editCanWrite = m.canWrite;
    if (m.friendId == this.editMemberId) {
      this.editMemberId = -1
    }
    else {
      this.editMemberId = m.friendId
    }
  }

  save(m: FavMember) {
    var friendID = this.editMemberId;
    var canRead = $('#read' + friendID).prop('checked');
    var canWrite = $('#write' + friendID).prop('checked');
    var memberName = ""; // It doesn't matter
    var favMember = new FavMember("", friendID, canRead, canWrite)
    favMember.setOwner(this.ownerID);

    if (this.shouldDelete(friendID)){
      this.memberService.deleteFavMember(favMember);
    } else if (this.isCreate(m)){
      this.memberService.createFavMember(favMember);
    } else{
      this.memberService.updateFavMember(favMember);
    } 

    m.canRead = canRead;
    m.canWrite = canWrite;
    this.initSingleSlider(m)
    this.toggle(m);
  }

  isCreate(m: FavMember){
    return !m.canRead && !m.canWrite
  }

  shouldDelete(id: number){
    var canRead = $('#read' + id).prop('checked');
    var canWrite = $('#write' + id).prop('checked');
    return !canRead && !canWrite
  }
}


