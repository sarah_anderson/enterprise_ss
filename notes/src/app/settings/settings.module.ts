import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './settings.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule, SettingsRoutingModule, FormsModule],
  declarations: [SettingsComponent],
  exports: [SettingsComponent]
})
export class SettingsModule {}
