import { Component, OnInit } from '@angular/core';
import { ViewbagService, ApplicationUser } from './viewbag.service';
import { NgForm } from '@angular/forms';
import { Router } from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  currentUser: string;
  currentUserIsAdmin: boolean;
  title: string;

  appUser: ApplicationUser
  username: string = "Sarah";
  password: string = "123";
  error: string = "";


  loading: boolean = false;

  constructor(private viewbagService: ViewbagService, private router: Router) {   
    this.currentUser = this.viewbagService.getUsername(); 
    this.currentUserIsAdmin  = this.viewbagService.isUserAdmin();
   

    this.appUser = this.viewbagService.getAppUser();
    this.currentUser = this.viewbagService.getUsername();

    this.currentUserIsAdmin = false;
    this.title = "Scott and Sarah's Enterprise Note Application";
  }

  ngOnInit(){
    this.currentUser = this.viewbagService.getUsername(); 
    this.currentUserIsAdmin  = this.viewbagService.isUserAdmin();
    this.appUser = this.viewbagService.getAppUser();
    this.currentUser = this.viewbagService.getUsername();
  }

  login = function () {
    this.loading = true;
    this.viewbagService.signin(this.username, this.password)
    .subscribe(
      (data) => {
        this.appUser = data;
        this.currentUserIsAdmin = this.appUser.isAdmin
        this.currentUser = this.appUser.username;
        
      },
      (err) => { },
      () =>{      
        this.currentUser = this.appUser.username;
        this.router.navigate(['/user'])
      }
    )
    
    this.loading = false;
  }

  logout = function () {
    this.currentUser = ""
    this.username = "";
    this.password = "";
    this.currentUserIsAdmin = false;
    this.viewbagService.logout()   
  }
}
