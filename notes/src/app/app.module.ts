import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule  } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';

import { MembersModule } from './members/members.module';
import { UserModule } from './user/user.module';
import { NotesModule } from './notes/notes.module';
import { SettingsModule } from './settings/settings.module';

import { MemberService } from './members/member.service';
import { NoteService } from './notes/note.service';
import { ViewbagService } from './viewbag.service';
import { AppComponent } from './app.component';
//import { UserComponent } from './user/user.component';
//import { CreateComponent } from './notes/create/create.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MembersModule,
    NotesModule,
    UserModule,
    SettingsModule,
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [MemberService, ViewbagService,NoteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
