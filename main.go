package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"bitbucket.org/sarah_anderson/enterprise_ss/server/database"
	"github.com/gorilla/mux"
)

// error response contains everything we need to use http.Error
type handlerError struct {
	Error   error
	Message string
	Code    int
}

// a custom type that we can use for handling errors and formatting responses
type handler func(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError)

// attach the standard ServeHTTP method to our handler so the http library can call it
func (fn handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// here we could do some prep work before calling the handler if we wanted to

	// call the actual handler
	response, err := fn(w, r)

	// check for errors
	if err != nil {
		log.Printf("ERROR: %v\n", err.Error)
		http.Error(w, fmt.Sprintf(`{"error":"%s"}`, err.Message), err.Code)
		return
	}
	if response == nil {
		log.Printf("ERROR: response from method is nil\n")
		http.Error(w, "Internal server error. Check the logs.", http.StatusInternalServerError)
		return
	}

	// turn the response into JSON
	bytes, e := json.Marshal(response)
	if e != nil {
		http.Error(w, "Error marshalling JSON", http.StatusInternalServerError)
		return
	}

	// send the response and log
	w.Header().Set("Content-Typse", "application/json")
	w.Write(bytes)
	log.Printf("%s %s %s %d", r.RemoteAddr, r.Method, r.URL, 200)

}

func main() {
	// setup routes
	router := mux.NewRouter()
	router.Handle("/login", handler(login)).Methods("POST", "OPTIONS")
	router.Handle("/member/list", handler(getMemberList)).Methods("GET", "OPTIONS")
	router.Handle("/member/create", handler(addNewMemberPOST)).Methods("POST", "OPTIONS")

	router.Handle("/notes/list", handler(getNoteList)).Methods("GET", "OPTIONS")
	router.Handle("/notes/create", handler(addNewNotePOST)).Methods("OPTIONS", "POST")
	router.Handle("/notes/edit", handler(editNotePOST)).Methods("POST", "OPTIONS")

	router.Handle("/favourite/list", handler(getFavouriteList)).Methods("GET", "OPTIONS")
	router.Handle("/favourite/create", handler(addNewFavouritePOST)).Methods("POST", "OPTIONS")
	router.Handle("/favourite/edit", handler(editFavouritePOST)).Methods("POST", "OPTIONS")
	router.Handle("/favourite/delete", handler(deleteFavouritePOST)).Methods("OPTIONS", "POST", "PUT")

	http.Handle("/", router)

	initResult := database.InitialiseDB()

	if initResult == true {
		database.CreateAllTables()
		database.CreatePrepares()

		log.Printf("Running on port 8080")
		panic(http.ListenAndServe(":8080", nil))
	} else {
		fmt.Println("Failed to open a connection to database.")
	}
}
