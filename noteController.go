package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"fmt"

	"bitbucket.org/sarah_anderson/enterprise_ss/server/database"
)

func getNoteList(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "OwnerID, Content-Type, Origin")
	
	noteFilterOptions := getSearchParameters(r)
	regex := getRegex(noteFilterOptions)
	ownerID := getOwnerID(r) 
	sessionID := getSessionID(r)                     
	notes := database.RegexQuery(ownerID, sessionID, regex, noteFilterOptions.IsNewQuery) 

	return notes, nil
}

func addNewNotePOST(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "OwnerID, Content-Type")

	ownerID := getOwnerID(r)

	if isValidUser(ownerID) {
		// Create a note
		body, _ := ioutil.ReadAll(r.Body)
		var viewModel AddNewMemberModel
		err := json.Unmarshal(body, &viewModel)
		
		if err != nil {
			fmt.Println("Cannot add new note:", err)
		} else {
			
			newNoteID := database.AddNewNote(&viewModel.NewNote)
			// Assign full rights to the owner of the note
			ownerMember := database.NoteMember{
				NoteID:     newNoteID,
				MemberID:   viewModel.NewNote.OwnerID,
				AccessType: 2,
			}
			database.AddNewNoteMember(&ownerMember)

			// Assign read only access
			for _, readOnlyID := range viewModel.ReadOnlyUsersIDs {
				readOnlyNoteMember := database.NoteMember{
					NoteID:     newNoteID,
					MemberID:   readOnlyID,
					AccessType: 1,
				}
				database.AddNewNoteMember(&readOnlyNoteMember)
			}

			// Assign read&write access
			for _, readWriteID := range viewModel.ReadWriteUsersIDs {
				readWriteNoteMember := database.NoteMember{
					NoteID:     newNoteID,
					MemberID:   readWriteID,
					AccessType: 2,
				}
				database.AddNewNoteMember(&readWriteNoteMember)
			}
		}
	}

	return "addNewNotePOST", nil
}

func editNotePOST(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "OwnerID, Content-Type")

	userID := getOwnerID(r)

	if isValidUser(userID) {
		// Create a note
		body, _ := ioutil.ReadAll(r.Body)
		var viewModel AddNewMemberModel
		err := json.Unmarshal(body, &viewModel)
		
		if err != nil {
			fmt.Println("Cannot edit new note:", err)
		} else {
			
			database.UpdateNote(&viewModel.NewNote)
			noteID := viewModel.NewNote.NoteID
			fmt.Println(noteID)
			
			if (userID == viewModel.NewNote.OwnerID) {
				database.DeleteNoteMember(noteID)

				// Assign full rights to the owner of the note
				ownerMember := database.NoteMember{
					NoteID:     noteID,
					MemberID:   viewModel.NewNote.OwnerID,
					AccessType: 2,
				}
				database.AddNewNoteMember(&ownerMember)
				// Assign read only access
				for _, readOnlyID := range viewModel.ReadOnlyUsersIDs {
					readOnlyNoteMember := database.NoteMember{
						NoteID:     noteID,
						MemberID:   readOnlyID,
						AccessType: 1,
					}
					database.AddNewNoteMember(&readOnlyNoteMember)
				}

				// Assign read&write access
				for _, readWriteID := range viewModel.ReadWriteUsersIDs {
					readWriteNoteMember := database.NoteMember{
						NoteID:     noteID,
						MemberID:   readWriteID,
						AccessType: 2,
					}
					database.AddNewNoteMember(&readWriteNoteMember)
				}
			}
		}
	}

	return "editNotePOST", nil
}

