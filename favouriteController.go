package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/sarah_anderson/enterprise_ss/server/database"
)

// GET Favourties
func getFavouriteList(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "OwnerID, Content-Type, name ")
	fmt.Println(r.Trailer)	
	for key, value := range r.Trailer {
		fmt.Println("Key:", key, "Value:", value)
	}
	userID := getOwnerID(r)
	fmt.Println("Using this userID: ", userID)
	if isValidUser(userID) {
		favMembers := database.GetAllMemberFavourites(userID)
		return favMembers, nil
	}

	return "Invalid User", nil
}


// Add a default favourite
func addNewFavouritePOST(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "OwnerID, Content-Type")

	ownerID := getOwnerID(r)
	if isValidUser(ownerID) {
		body, _ := ioutil.ReadAll(r.Body)
		fmt.Println(body)
		var favourite database.MemberDefaultFavourite
		err := json.Unmarshal(body, &favourite)
		if err != nil {
			fmt.Println("Error adding favourite")
		} else {
			fmt.Println("Trying to add: ", favourite, favourite.MemberID)
			database.AddNewMemberDefaultFavourite(&favourite)
			fmt.Println("Added favourite")
		}
	}

	return "addNewMemberPOST", nil
}

// PUT: Edit favourite
func editFavouritePOST(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "OwnerID, Content-Type")
	ownerID := getOwnerID(r)
	if isValidUser(ownerID) {
		body, _ := ioutil.ReadAll(r.Body)
		var favourite database.MemberDefaultFavourite
		err := json.Unmarshal(body, &favourite)
		if err != nil {
			fmt.Println("Error adding favourite")
		} else {
			database.UpdateMemberFavourite(&favourite)
			fmt.Println("Added favourite")
		}
	}

	return "editFavouritePOST", nil
}

func deleteFavouritePOST(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, PUT")
	w.Header().Set("Access-Control-Allow-Headers", "OwnerID, Content-Type")

	fmt.Println("deleteFavouritePOST")
	ownerID := getOwnerID(r)
	if isValidUser(ownerID) {
		body, _ := ioutil.ReadAll(r.Body)
		var favourite database.MemberDefaultFavourite
		err := json.Unmarshal(body, &favourite)
		if err != nil {
			fmt.Println("Error delete favourite")
		} else {
			database.DeleteMemberDefaultFavourite(&favourite)
			fmt.Println("Delete favourite")
		}
	}

	return "deleteMemberPOST", nil
}