# Sarah and Scott Assignment Two

Task Seperations:

Write structs
routes, parameters and results ... shape of the json that will be sent to the client



### Login 
* Active list of login sessions in the database identified by username/Id
* The website is going to keep hold of you username/id on the client to make requests for your user
* No interface to create an admin. 

### Members
* List Members
* Create member only if you're an admin
* Edit Members details only if you're admin

### Permissions Preferences
* Default reader writes

### Notes
* List notes
* Create note
    Fields:
        - Create content 
        - Select your read-write users
    Validation:
        - Content is not null
* View note
* Edit note
    Validation:
    - if must be: The owner or have write access

Whenever a client logs in, they will receive a unique token which they will use when performing any actions
which communicate with the server. e.g. if sending a request to make a new note, send the note data AND the token key.





