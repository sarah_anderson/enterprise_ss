package database

import (
	"fmt"
	"database/sql"
	"log"

	_ "github.com/lib/pq"
)

type NoteListItem struct {
	Owner           string
	Content         string
	CreatedDateTime string
}

type NoteFilterOptions struct {
	IsNewQuery    bool
	PreSuffixText string
	AsPrefix      bool
	AsSuffix      bool

	Numbers       int64
	IsConsecutive bool

	EmailDomain string

	SearchPresetWords bool

	CapWord3OrMore bool
}

// Note, these need to have capital letters
type Member struct {
	MemberID    int
	Name        string
	AccountType byte
	Password    string
}

type Note struct {
	NoteID          int
	OwnerID         int
	Text            string
	CreatedDateTime string
}

type NoteMember struct {
	NoteID     int
	MemberID   int
	AccessType byte
}

type MemberDefaultFavourite struct {
	MemberID int
	FriendID int
	Access   int
}

/*This is needed for the view*/
type MemberFavourite struct {
	MemberID   int
	FriendID   int
	FriendName string
	Access     byte
}

type MemberUser struct {
	MemberID    int
	Name        string
	AccountType byte
}

type RegexResult struct {
	NoteID     int
	MemberID   int
	MemberName string
	NoteText   string
	OccCount   int
	Access     byte
}

/*
type DBConnectionParams struct {
	Username string
	Password string
	DBHost   string
	DBPort   string
	Database string
}*/

//var members []Member
var notes []Note

var db *sql.DB

//var db_params DBConnectionParams
var err error

var prep_selallmember *sql.Stmt
var prep_selnotea *sql.Stmt
var prep_selnoteb *sql.Stmt
var prep_selmemfavs *sql.Stmt
var prep_selallmemfavs *sql.Stmt
var prep_selmemberuser *sql.Stmt
var prep_validateuser *sql.Stmt

var prep_insmember *sql.Stmt
var prep_insnote *sql.Stmt
var prep_insnotemember *sql.Stmt
var prep_insmemberfav *sql.Stmt

var prep_updmember *sql.Stmt
var prep_updnote *sql.Stmt
var prep_updnotemember *sql.Stmt
var prep_updmemberfav *sql.Stmt

var prep_delmember *sql.Stmt
var prep_delnote *sql.Stmt
var prep_delnotemember *sql.Stmt
var prep_delmemberfav *sql.Stmt
var prep_delregexresults *sql.Stmt

/*
	1. Connect to database								done
	2. Create prepared statements						done
	3. Create tables if they don't already exist		done
	4. Insert, Update, Delete data from the database 	Inserts done, Updates done, member & regex_results delete done
	5. Regex											done

	Notes:

	27/09/17
		Added create table functions.
		Aggregated create table functions into one function.
		Added createPrepares function
		Added addNewNote function

	20/10/2017
		Fixed prepare statements (changed type to *sql.Stmt)
		Updated connection string to use for remote access
		Updated initialiseDB to ping db to see if its connected or not, method returns false if it falied to connect/errors
		Fixes to all add and update functions


	10/11/2017
		Need to fix 2nd SQL select statement in RegexQuery
*/

/*
	0 - no access
	1 - read only
	2 - everything (read + write)
*/
/*Request: Select notes where MemberID has at least read access  or they own the note (that satisy some search parameters)*/

/*Request: Select all favourites where MemberID = $1, but return in this format (see struct): MemberFavourite */

func InitialiseDB() bool {

	db, err = sql.Open("postgres", "user=postgres password=postgres dbname=postgres sslmode=disable")
	//db, err = sql.Open("postgres", "user=postgres password=EITesdC10 dbname=postgres host=107.174.128.151 port=9905 sslmode=disable")
	//db, err = sql.Open("postgres", "user=postgres password=postgres dbname=scottsarah sslmode=disable")

	if db == nil {
		//log.Fatal(err)
		return false
	}
	if err != nil {
		log.Fatal(err)
		return false
	} else {
		err1 := db.Ping()
		if err1 != nil {
			log.Fatal(err1)
			return false
		} else {
			return true
		}
	}
}

func CreatePrepares() {
	//Selects
	prep_selallmember, err = db.Prepare("SELECT member_ID, name, account_type FROM members")
	if err != nil {
		log.Fatal("Failed to create prepared statement for select all members. ", err)
	}

	prep_selnotea, err = db.Prepare("SELECT * FROM notes")
	if err != nil {
		log.Fatal("Failed to create prepared statement for select note a. ", err)
	}

	prep_selnoteb, err = db.Prepare("SELECT DISTINCT note_members.note_id, owner_id, text, created_datetime FROM notes INNER JOIN note_members ON notes.note_id = note_members.note_id WHERE member_id = $1 AND note_members.access_type <= $2")

	if err != nil {
		log.Fatal("Failed to create prepared statement for select note b. ", err)
	}

	prep_selmemfavs, err = db.Prepare("SELECT member_defaultfavourites.member_id, member_defaultfavourites.friend_id, members.name, member_defaultfavourites.access FROM member_defaultfavourites INNER JOIN members ON member_defaultfavourites.friend_id = members.member_id WHERE member_defaultfavourites.member_id = $1")
	if err != nil {
		log.Fatal("Failed to create prepared statement for select member favourites. ", err)
	}

	prep_selallmemfavs, err = db.Prepare("SELECT members.member_id as member_id, members.name as name, coalesce(fav.access, 0) as access FROM ( SELECT members.member_id as friend_id, members.name, member_defaultfavourites.access as access FROM member_defaultfavourites JOIN members ON member_defaultfavourites.friend_id = members.member_Id WHERE member_defaultfavourites.member_id = $1) fav FULL OUTER JOIN members ON fav.friend_id = members.member_Id ORDER BY access desc, members.name")
	if err != nil {
		log.Fatal("Failed to create prepared statement for select all member favourites. ", err)
	}

	prep_selmemberuser, err = db.Prepare("SELECT member_id, account_type FROM members WHERE name = $1")
	if err != nil {
		log.Fatal("Failed to create prepared statement for select member by username. ", err)
	}

	prep_validateuser, err = db.Prepare("SELECT * FROM members WHERE name = $1 AND password = $2")
	if err != nil {
		log.Fatal("Failed to create prepared statement for select member by username and password. ", err)
	}

	//Insertsdatetime
	prep_insmember, err = db.Prepare("INSERT INTO members (name, account_type, password) VALUES($1, $2, $3)")
	if err != nil {
		log.Fatal("Failed to create prepared statement for member insert. ", err)
	}

	prep_insnote, err = db.Prepare("INSERT INTO notes (owner_id, text, created_datetime) VALUES($1, $2, $3) RETURNING note_id")
	if err != nil {
		log.Fatal("Failed to create prepared statement for note insert. ", err)
	}

	prep_insnotemember, err = db.Prepare("INSERT INTO note_members VALUES($1, $2, $3)")
	if err != nil {
		log.Fatal("Failed to create prepared statement for note member insert. ", err)
	}

	prep_insmemberfav, err = db.Prepare("INSERT INTO member_defaultfavourites VALUES($1, $2, $3)")
	if err != nil {
		log.Fatal("Failed to create prepared statement for member favourite insert. ", err)
	}

	//Updates
	prep_updmember, err = db.Prepare("UPDATE members SET name = $1, account_type = $2 WHERE member_id = $3")
	if err != nil {
		log.Fatal("Failed to create prepared statement for member update. ", err)
	}

	prep_updnote, err = db.Prepare("UPDATE notes SET text = $1 WHERE note_id = $2")
	if err != nil {
		log.Fatal("Failed to create prepared statement for note update. ", err)
	}

	prep_updnotemember, err = db.Prepare("UPDATE note_members SET access_type = $1 WHERE note_id = $2 AND member_id = $3")
	if err != nil {
		log.Fatal("Failed to create prepared statement for note member update. ", err)
	}

	prep_updmemberfav, err = db.Prepare("UPDATE member_defaultfavourites SET access = $1 WHERE member_id = $2 AND friend_id = $3")
	if err != nil {
		log.Fatal("Failed to create prepared statement for member favourite update. ", err)
	}

	//Deletes
	prep_delmember, err = db.Prepare("DELETE FROM members WHERE member_id = $1")
	if err != nil {
		log.Fatal("Failed to create prepared statement for member delete. ", err)
	}

	prep_delnote, err = db.Prepare("DELETE FROM notes WHERE note_id = $1")
	if err != nil {
		log.Fatal("Failed to create prepared statement for note delete. ", err)
	}

	prep_delnotemember, err = db.Prepare("DELETE FROM note_members WHERE note_id = $1")
	if err != nil {
		log.Fatal("Failed to create prepared statement for note member delete. ", err)
	}

	prep_delmemberfav, err = db.Prepare("DELETE FROM member_defaultfavourites WHERE member_id = $1 AND friend_id = $2")
	if err != nil {
		log.Fatal("Failed to create prepared statement for member favourite delete. ", err)
	}

	prep_delregexresults, err = db.Prepare("DELETE FROM regex_results WHERE member_id = $1")
	if err != nil {
		log.Fatal("Failed to create prepared statement for regex_results delete. ", err)
	}
}

func CreateAllTables() {
	_, createSequenceErr := db.Exec(`CREATE SEQUENCE IF NOT EXISTS member_id_seq;`)
	if createSequenceErr != nil {
		panic(createSequenceErr)
	}
	_, err1 := db.Exec("CREATE TABLE IF NOT EXISTS members (member_id int, name text, account_type smallint, password text)")
	if err1 != nil {
		log.Fatal(err1)
	} else {
		fmt.Println("members table created.")
	}

	_, alterError := db.Exec(`ALTER SEQUENCE member_id_seq OWNED BY members.member_id;`)
	if alterError != nil {
		panic(alterError)
	}

	_, createSequenceErr = db.Exec(`CREATE SEQUENCE IF NOT EXISTS  note_id_seq;`)
	if createSequenceErr != nil {
		panic(createSequenceErr)
	}	
	_, err2 := db.Exec("CREATE TABLE IF NOT EXISTS notes (note_id int NOT NULL DEFAULT nextval('note_id_seq'), owner_id int, text text, created_datetime timestamp)")
	if err2 != nil {
		log.Fatal(err2)
	} else {
		fmt.Println("notes table created.")
	}

	_, alterError = db.Exec(`ALTER SEQUENCE note_id_seq OWNED BY notes.note_id;`)
	if alterError != nil {
		panic(alterError)
	}

	_, err3 := db.Exec("CREATE TABLE IF NOT EXISTS note_members (note_id int, member_id int, access_type smallint)")
	if err3 != nil {
		log.Fatal(err3)
	} else {
		fmt.Println("note_members table created.")
	}

	_, err4 := db.Exec("CREATE TABLE IF NOT EXISTS member_defaultfavourites (member_id int, friend_id int, access smallint)")
	if err4 != nil {
		log.Fatal(err4)
	} else {
		fmt.Println("member_defaultfavourites table created.")
	}

	_, err5 := db.Exec("CREATE TABLE IF NOT EXISTS regex_results (member_id int, session_id int, note_id int, regex_result text, occurrences int)")
	if err5 != nil {
		log.Fatal(err5)
	} else {
		fmt.Println("regex_results table created.")
	}
}

func WipeRegexResults() {

}

func GetAllMembers() []Member {
	var members []Member
	//rows, err1 := db.Query("SELECT * FROM Members")
	rows, err1 := prep_selallmember.Query()
	if err1 != nil {
		panic(err1)
	}

	var (
		memberID    int
		name        string
		accountType byte
	)

	for rows.Next() {
		err2 := rows.Scan(&memberID, &name, &accountType)
		if err2 != nil {
			panic(err2)
		}

		newMember := Member{
			MemberID:    memberID,
			Name:        name,
			AccountType: accountType,
		}
		members = append(members, newMember)
	}
	rows.Close()
	return members
}

func GetNotesWithMemberAccess(memberID int, accessType byte) []Note {
	var retNotes []Note
	rows, err1 := prep_selnoteb.Query(memberID, accessType)
	if err1 != nil {
		panic(err1)
	}

	var (
		noteID          int
		ownerID         int
		text            string
		createdDateTime string
	)

	for rows.Next() {
		err2 := rows.Scan(&noteID, &ownerID, &text, &createdDateTime)
		if err2 != nil {
			panic(err2)
		}

		newNote := Note{
			NoteID:          noteID,
			OwnerID:         ownerID,
			Text:            text,
			CreatedDateTime: createdDateTime,
		}
		retNotes = append(notes, newNote)
	}
	rows.Close()
	return retNotes
}

func GetMemberFavourites(memberID int) []MemberFavourite {
	var memFavs []MemberFavourite
	rows, err1 := prep_selmemfavs.Query(memberID)
	if err1 != nil {
		panic(err1)
	}

	var (
		memberIDB  int
		friendID   int
		friendName string
		access     byte
	)

	for rows.Next() {
		err2 := rows.Scan(&memberIDB, &friendID, &friendName, &access)
		if err2 != nil {
			panic(err2)
		}

		newMemFav := MemberFavourite{
			MemberID:   memberIDB,
			FriendID:   friendID,
			FriendName: friendName,
			Access:     access,
		}
		memFavs = append(memFavs, newMemFav)
	}
	rows.Close()
	return memFavs
}

func GetAllMemberFavourites(memberID int) []MemberFavourite {
	var memFavs []MemberFavourite
	rows, err1 := prep_selallmemfavs.Query(memberID)
	if err1 != nil {
		panic(err1)
	}

	var (
		friendID   int
		friendName string
		access     byte
	)

	for rows.Next() {
		err2 := rows.Scan(&friendID, &friendName, &access)
		if err2 != nil {
			panic(err2)
		}

		newMemFav := MemberFavourite{
			FriendID:   friendID,
			FriendName: friendName,
			Access:     access,
		}
		memFavs = append(memFavs, newMemFav)
	}
	rows.Close()
	return memFavs
}

func GetMemberByUsername(username string) MemberUser {
	var newMember MemberUser

	rows, err1 := prep_selmemberuser.Query(username)
	if err1 != nil {
		panic(err1)
	}

	var (
		memberID    int
		accountType byte
	)

	if rows.Next() {
		err2 := rows.Scan(&memberID, &accountType)
		if err2 != nil {
			panic(err2)
		}

		newMember = MemberUser{
			MemberID:    memberID,
			Name:        username,
			AccountType: accountType,
		}
	} else {
		fmt.Println("Failed to find member: ", username)
	}
	rows.Close()
	return newMember
}

func ValidateMemberLogin(username string, password string) bool {
	rows, err1 := prep_validateuser.Query(username, password)
	if err1 != nil {
		panic(err1)
	}

	if rows.Next() {
		rows.Close()
		return true
	} else {
		rows.Close()
		return false
	}
}

func AddMember(member *Member) {
	tx, beginErr := db.Begin()
	if beginErr != nil {
		panic(beginErr)
	}
	defer tx.Rollback()

	resExe, execErr := prep_insmember.Exec(member.Name, member.AccountType, member.Password) //name, account type. password?
	if execErr != nil {
		panic(execErr)
	}

	_, rowsErr := resExe.RowsAffected() //rowCount
	if rowsErr != nil {
		panic(rowsErr)
	}

	tx.Commit()
	log.Printf("Added member: ID: %d Name: %s", member.MemberID, member.Name)
}

func AddNewNote(note *Note) int {
	var note_id int

	tx, beginErr := db.Begin()
	if beginErr != nil {
		panic(beginErr)
	}
	defer tx.Rollback()

	res, err1 := prep_insnote.Query(note.OwnerID, note.Text, note.CreatedDateTime) //note_id assigned / incremented automatically in database
	if err1 != nil {
		fmt.Println(err1) //not fatal, just log error.
	}

	if res.Next() == true {
		err2 := res.Scan(&note_id)
		if err2 != nil {
			fmt.Println(err2)
			note_id = -1
		}
	} else {
		note_id = -1
	}

	tx.Commit()
	log.Printf("Added note: ", note_id, note.OwnerID, note.Text)
	return note_id
}

func AddNewNoteMember(noteMember *NoteMember) {
	tx, beginErr := db.Begin()
	if beginErr != nil {
		panic(beginErr)
	}
	defer tx.Rollback()

	res, err1 := prep_insnotemember.Exec(noteMember.NoteID, noteMember.MemberID, noteMember.AccessType)
	if err1 != nil {
		fmt.Println(err1)
	}

	_, rowsErr := res.RowsAffected()
	if rowsErr != nil {
		panic(rowsErr)
	}

	tx.Commit()
	log.Printf("Added note member: ", noteMember.NoteID, noteMember.MemberID, noteMember.AccessType)
}

func AddNewMemberDefaultFavourite(memberFav *MemberDefaultFavourite) {
	tx, beginErr := db.Begin()
	if beginErr != nil {
		panic(beginErr)
	}
	defer tx.Rollback()

	res, err1 := prep_insmemberfav.Exec(memberFav.MemberID, memberFav.FriendID, memberFav.Access)
	if err1 != nil {
		fmt.Println(err1)
	}

	_, rowsErr := res.RowsAffected()
	if rowsErr != nil {
		panic(rowsErr)
	}

	tx.Commit()
	log.Printf("Added member favourite: ", memberFav.MemberID, memberFav.FriendID, memberFav.Access)
}

func UpdateMember(member *Member) {
	tx, beginErr := db.Begin()
	if beginErr != nil {
		panic(beginErr)
	}
	defer tx.Rollback()
	//"UPDATE members SET name = $1, account_type = $2 WHERE member_id = $3)"
	res, err1 := prep_updmember.Exec(member.Name, member.AccountType, member.MemberID)
	if err1 != nil {
		fmt.Println(err1)
	}

	_, rowsErr := res.RowsAffected()
	if rowsErr != nil {
		panic(rowsErr)
	}

	tx.Commit()
	log.Printf("Updated member: ", member.MemberID, member.Name, member.AccountType)
}

func DeleteMemberDefaultFavourite(memberFav *MemberDefaultFavourite) {
	tx, beginErr := db.Begin()
	if beginErr != nil {
		panic(beginErr)
	}
	defer tx.Rollback()

	fmt.Println("Trying to delete: MemberID: ", memberFav.MemberID, " Friend: ", memberFav.FriendID)
	res, err1 := prep_delmemberfav.Exec(memberFav.MemberID, memberFav.FriendID)
	if err1 != nil {
		fmt.Println(err1)
	}

	_, rowsErr := res.RowsAffected()
	if rowsErr != nil {
		panic(rowsErr)
	}

	tx.Commit()
	log.Printf("Deleted member favourite: ", memberFav.MemberID, memberFav.FriendID)
}

func UpdateNote(note *Note) {
	tx, beginErr := db.Begin()
	if beginErr != nil {
		panic(beginErr)
	}
	defer tx.Rollback()
	//"UPDATE notes SET text = $1 WHERE note_id = $2)"
	res, err1 := prep_updnote.Exec(note.Text, note.NoteID)
	if err1 != nil {
		fmt.Println(err1)
	}

	_, rowsErr := res.RowsAffected() //rowCount
	if rowsErr != nil {
		panic(rowsErr)
	}

	tx.Commit()
	log.Printf("Updated note: ", note.NoteID, note.OwnerID, note.Text, note) //print whole note data or just updated parts?
}

func UpdateNoteMember(noteMember *NoteMember) {
	tx, beginErr := db.Begin()
	if beginErr != nil {
		panic(beginErr)
	}
	defer tx.Rollback()
	//"UPDATE note_members SET access_type = $1 WHERE note_id = $2 AND member_id = $3)"
	res, err1 := prep_updmember.Exec(noteMember.AccessType, noteMember.NoteID, noteMember.MemberID)
	if err1 != nil {
		fmt.Println(err1)
	}

	_, rowsErr := res.RowsAffected() //rowCount
	if rowsErr != nil {
		panic(rowsErr)
	}

	tx.Commit()
	log.Printf("Updated note member: ", noteMember.NoteID, noteMember.MemberID, noteMember.AccessType)
}

func UpdateMemberFavourite(memberFav *MemberDefaultFavourite) {
	tx, beginErr := db.Begin()
	if beginErr != nil {
		panic(beginErr)
	}
	defer tx.Rollback()
	//"UPDATE member_defaultfavourites SET access = $1 WHERE member_id = $2 AND friend_id = $3)"
	res, err1 := prep_updmemberfav.Exec(memberFav.Access, memberFav.MemberID, memberFav.FriendID)
	if err1 != nil {
		fmt.Println(err1)
	}

	_, rowsErr := res.RowsAffected() //rowCount
	if rowsErr != nil {
		panic(rowsErr)
	}

	tx.Commit()
	log.Printf("Updated member favourite: ", memberFav.MemberID, memberFav.FriendID, memberFav.Access)
}

func DeleteMember(memberID int32) {
	_, err1 := prep_delmember.Exec(memberID)
	if err1 != nil {
		fmt.Println(err1)
	}
}

func DeleteRegexResults(memberID int) {
	_, err1 := prep_delregexresults.Exec(memberID)
	if err1 != nil {
		fmt.Println(err1)
	}
}

func DeleteNoteMember(noteID int) {
	_, err1 := prep_delnotemember.Exec(noteID)
	if err1 != nil {
		fmt.Println(err1)
	}
}

func RegexQuery(memberID int, sessionID int, searchParam string, newQuery bool) []RegexResult {
	var regexResults []RegexResult
	var rows *sql.Rows
	//only notes that you have read/write access to

	if newQuery == true {
		rows, err = db.Query(`SELECT RegResults.note_id, RegResults.text, COUNT(*), RegResults.ownerName, note_members.access_type as access
		FROM ( 
			SELECT note_id, text, (regexp_matches(TEXT, $1)), members.name as ownerName
			FROM notes 
			INNER JOIN members ON notes.owner_id = members.member_id	
		) RegResults 
		INNER JOIN note_members ON note_members.member_id = $2 AND note_members.note_id = RegResults.note_id 
		GROUP BY 
		RegResults.text, RegResults.note_id, RegResults.ownerName, access;
		`, searchParam, memberID)
		if err != nil {
			panic(err)
		}
	} else {
		rows, err = db.Query(`SELECT RegResults.note_id, RegResults.regex_result, RegResults.occurrences, COUNT(*), RegResults.ownerName, note_members.access_type as access 
        FROM (
            SELECT note_id, regex_result, occurrences, (regexp_matches(regex_result, $1)), members.name as ownerName 
            FROM regex_results 
            INNER JOIN members ON regex_results.member_id = members.member_id 
        ) RegResults 
        INNER JOIN note_members ON note_members.member_id = $2 AND note_members.note_id = RegResults.note_id
        GROUP BY RegResults.regex_result, RegResults.note_id, RegResults.occurrences, RegResults.ownerName, note_members.access_type;`, searchParam, memberID)
		if err != nil {
			panic(err)
		}
	}
	var (
		noteID    int
		noteText  string
		occCount  int
		ownerName string
		access    byte
		occCountB int
	)

	fmt.Println(memberID, sessionID)
	db.Exec("DELETE FROM regex_results WHERE member_id = $1 AND session_id = $2", memberID, sessionID)
	

	for rows.Next() {
		if newQuery == true {
			err2 := rows.Scan(&noteID, &noteText, &occCount, &ownerName, &access)
			if err2 != nil {
				panic(err2)
			}
		} else {
			err2 := rows.Scan(&noteID, &noteText, &occCount, &occCountB, &ownerName, &access)
			if err2 != nil {
				panic(err2)
			}
		}

		tx, err3 := db.Begin()
		if err3 != nil {
			panic(err3)
		}

		stmt, err4 := tx.Prepare("INSERT INTO regex_results (member_id, session_id, note_id, regex_result, occurrences) VALUES ($1, $2, $3, $4, $5)")
		if err4 != nil {
			panic(err4)
		}
		_, err5 := stmt.Exec(memberID, sessionID, noteID, noteText, occCount+occCountB)
		if err5 != nil {
			panic(err5)
		}
		err6 := tx.Commit()
		if err6 != nil {
			log.Fatal(err6)
		}

		newRegexResult := RegexResult{
			NoteID:     noteID,
			MemberID:   memberID,
			MemberName: ownerName,
			NoteText:   noteText,
			OccCount:   occCount,
			Access:     access,
		}
		regexResults = append(regexResults, newRegexResult)
	}
	rows.Close()

	return regexResults
}
