package main

import (
	"bitbucket.org/sarah_anderson/enterprise_ss/server/database"
)

type AddNewMemberModel struct {
	ReadOnlyUsersIDs  []int
	ReadWriteUsersIDs []int
	NewNote           database.Note
}


func memberIsValid(member *database.Member) bool {
	return (len(member.Name) != 0 && (member.AccountType == 0 || member.AccountType == 1) && len(member.Password) != 0)
}