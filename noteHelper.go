package main

import (
	"strconv"
	"net/http"
	"net/url"
	"bitbucket.org/sarah_anderson/enterprise_ss/server/database"
)


func getSearchParameters(r *http.Request) database.NoteFilterOptions {
	params, _ := url.ParseQuery(r.URL.RawQuery)
	isNewQuery := false
	preSuffixText := ""
	asPrefix := false
	asSuffix := false
	var numbers int64
	numbers, _ = strconv.ParseInt("0", 10, 32) // Not true
	isConsecutive := false
	emailDomain := ""
	searchPresetWords := false
	capWord3OrMore := false
	var err error
	if val, ok := params["preSufFixText"]; ok {
		preSuffixText = val[0]
	}

	if val, ok := params["asPrefix"]; ok {
		asPrefix, err = strconv.ParseBool(val[0])
		if err != nil {
			asPrefix = false
		}
	}

	if val, ok := params["asSuffix"]; ok {
		asSuffix, err = strconv.ParseBool(val[0])
		if err != nil {
			asSuffix = false
		}
	}

	if val, ok := params["numbers"]; ok {
		numbers, err = strconv.ParseInt(val[0], 10, 32)
		if err != nil {
			numbers, _ = strconv.ParseInt("0", 10, 32) // Not true
		}
	}

	if val, ok := params["isConsecutive"]; ok {
		isConsecutive, err = strconv.ParseBool(val[0])
		if err != nil {
			isConsecutive = false
		}
	}

	if val, ok := params["emailDomain"]; ok {
		emailDomain = val[0]
	}

	if val, ok := params["searchPresetWords"]; ok {
		searchPresetWords, err = strconv.ParseBool(val[0])
		if err != nil {
			searchPresetWords = false
		}
	}

	if val, ok := params["capWord3OrMore"]; ok {
		capWord3OrMore, err = strconv.ParseBool(val[0])
		if err != nil {
			capWord3OrMore = false
		}
	}

	if val, ok := params["isNewQuery"]; ok {
		isNewQuery, err = strconv.ParseBool(val[0])
		if err != nil {
			isNewQuery = false
		}
	}

	noteFilterOptions := database.NoteFilterOptions{
		PreSuffixText:     preSuffixText,
		AsPrefix:          asPrefix,
		AsSuffix:          asSuffix,
		Numbers:           numbers,
		IsConsecutive:     isConsecutive,
		EmailDomain:       emailDomain,
		SearchPresetWords: searchPresetWords,
		CapWord3OrMore:    capWord3OrMore,
		IsNewQuery: 		isNewQuery,
	}

	return noteFilterOptions
}



func getRegex(noteFilterOptions database.NoteFilterOptions) string {
	/*fmt.Println(noteFilterOptions.PreSuffixText)
	fmt.Println(noteFilterOptions.AsPrefix)
	fmt.Println(noteFilterOptions.AsSuffix)
	fmt.Println(noteFilterOptions.Numbers)
	fmt.Println(noteFilterOptions.IsConsecutive)
	fmt.Println(noteFilterOptions.EmailDomain)
	fmt.Println(noteFilterOptions.SearchPresetWords)
	fmt.Println(noteFilterOptions.CapWord3OrMore)
	fmt.Println(noteFilterOptions.IsNewQuery)*/
	if (noteFilterOptions.PreSuffixText != ""){
		if (noteFilterOptions.AsPrefix && noteFilterOptions.AsSuffix){
			return `\w*` + noteFilterOptions.PreSuffixText
		}

		if (noteFilterOptions.AsPrefix){
			return `\w*` + noteFilterOptions.PreSuffixText + `\w*`
		}

		if (noteFilterOptions.AsSuffix){
			return `\w*` + noteFilterOptions.PreSuffixText + `$`
		}		
	}

	if (noteFilterOptions.Numbers != 0){
		if (noteFilterOptions.IsConsecutive){
			return `@"^\d$`
		}
		return `(?([0-9]{2,3}))?([ .-]?)([0-9]{3,4})\2([0-9]{4})`
	}

	if (noteFilterOptions.EmailDomain != ""){
		return `.?@.?(` + noteFilterOptions.EmailDomain + `).?`
	}

	if (noteFilterOptions.SearchPresetWords){
		return `meeting, minutes, agenda, action, attendees, apologies (meeting|minutes|agenda|action|attendees|apologies)/gmi`
	}
	
	if (noteFilterOptions.CapWord3OrMore){
		return `([A-Z]+){3,}`
	}

	return "."
}