package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/sarah_anderson/enterprise_ss/server/database"
)

// GET: Member List
func getMemberList(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, OwnerID, Content-Type")
	
	userID := getOwnerID(r)
	

	if isValidUser(userID) && isAdminUser(userID) {
		members := database.GetAllMembers()
		return members, nil
	}

	return "Invalid User", nil
}

// POST: Add a Member ADMIN ONLY
func addNewMemberPOST(w http.ResponseWriter, r *http.Request) (interface{}, *handlerError) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "OwnerID, Content-Type")

	ownerID := -1
	if isValidUser(ownerID) && isAdminUser(ownerID) {
		body, _ := ioutil.ReadAll(r.Body)
		var member database.Member
		err := json.Unmarshal(body, &member)
		if err != nil {
			fmt.Println("Error adding member")
			return "Error adding member", nil
		}

		if memberIsValid(&member) {
			encrypt(&member.Password)
			database.AddMember(&member)
			fmt.Println("Added member")
			return "Added member", nil
		} else {
			fmt.Println(member)
			fmt.Println("Member is not valid")
			return "Member is not valid", nil
		}
	}

	return "No access", nil
}

