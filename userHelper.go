package main

import (
	"net/http"
	"strconv"
	//"encoding/json"
	//"io/ioutil"
	"fmt"
	"net/url"
	"bitbucket.org/sarah_anderson/enterprise_ss/server/database"
)

type UserSignIn struct {
	Username string
	Password string
}

type ApplicationUser struct {
	UserId int;
	Username string;
	IsAdmin bool;
	Token string;
}

func isValidUser(id int) bool {
	return true
}

func hasRightsToEdit(id int, noteID int) bool {
	return true
}

func isAdminUser(id int) bool { 
	return true
}

func isValidUsernameAndPassword(username string, password string) bool{
	return database.ValidateMemberLogin(username, password)
}

func generateToken(username string) string{
	return "validTokenFor" + username
}

func validateToken(tokenString string, username string) bool{
	expectedTokenString := "validTokenFor" + username
	return (expectedTokenString == tokenString)
}

func getUserIdAndPrivileges(username string) (int, bool){
	member := database.GetMemberByUsername(username)
	isAdmin := member.AccountType == 1
	return member.MemberID, isAdmin
}

func generateAppUser(username string) ApplicationUser{
	userId, isAdminUser := getUserIdAndPrivileges(username)
	token := generateToken(username)
	return ApplicationUser{
		UserId: userId,
		Username: username,
		IsAdmin: isAdminUser,
		Token: token,
	}
}

func getOwnerID(r *http.Request) int{
	ua := r.Header.Get("OwnerID")
	ownerID, err := strconv.ParseInt(ua, 10, 64)
	if err != nil {
		ownerID = -2
	}
	fmt.Println(ownerID)
	return int(ownerID)
}

func encrypt(text *string) {
	// TO DO: Encrypt

}

func getSessionID(r *http.Request) int{
	params, _ := url.ParseQuery(r.URL.RawQuery)
	var sessionID int64
	sessionID = -1
	if val, ok := params["sessionID"]; ok {
		var	err error
		sessionID, err = strconv.ParseInt(val[0], 10, 64)
		if err != nil {
			sessionID = -1
		}
	}
	return int(sessionID)
}
